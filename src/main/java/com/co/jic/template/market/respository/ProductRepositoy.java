package com.co.jic.template.market.respository;

import com.co.jic.template.market.DAO.ProductDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepositoy extends JpaRepository<ProductDAO, Long> {
    ProductDAO findByName(String name);
    ProductDAO findByPrice(Double price);
}
