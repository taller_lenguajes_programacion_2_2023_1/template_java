package com.co.jic.template.market;

import com.co.jic.template.market.domain.product.Product;
import com.co.jic.template.market.domain.product.ProductCode;
import com.co.jic.template.market.domain.product.ProductName;
import com.co.jic.template.market.domain.product.ProductPrice;

public class MainTest {
    public static void main(String[] args) {
        System.out.println("Hola mundo");
        try {
            ProductName productName = new ProductName("Tostadas");
            ProductPrice productPrice = new ProductPrice(100d);
            ProductCode productCode = new ProductCode(123L);

            Product product = new Product(productName, productCode, productPrice);

            System.out.println(productName);
        } catch (NullPointerException e) {
            System.out.println(e.getMessage());
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
