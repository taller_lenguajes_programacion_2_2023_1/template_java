package com.co.jic.template.market.domain.product;

import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.Validate;
@Getter
@ToString
public class ProductName {
    private final String value;

    public ProductName(String value) {
        Validate.notNull(value,"El nombre del producto no puedes ser nulo");
        Validate.isTrue(value.length() <= 30, "El nombre del producto no puede ser mayor a 30 caracteres");
        this.value = value;
    }
}
