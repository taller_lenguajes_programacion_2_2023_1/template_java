package com.co.jic.template.market.domain.product;

import lombok.AllArgsConstructor;
import lombok.Data;

//value object ***
@Data
@AllArgsConstructor
public class Product {
    private ProductName name;
    private ProductCode id;
    private ProductPrice price;
}
