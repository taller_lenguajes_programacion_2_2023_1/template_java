package com.co.jic.template.market.DTO;

import lombok.Data;

@Data
public class UserDTO {
    private String user;
    private String pass;
    private Boolean status;
}
