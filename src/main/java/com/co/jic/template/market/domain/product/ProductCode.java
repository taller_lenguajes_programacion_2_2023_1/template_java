package com.co.jic.template.market.domain.product;

import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.Validate;

@Getter
@ToString
public class ProductCode {
    private final Long value;

    public ProductCode(Long value) {
        Validate.notNull(value, "El codigo del producto no puede ser nulo");
        Validate.isTrue(value != 0);
        this.value = value;
    }
}
