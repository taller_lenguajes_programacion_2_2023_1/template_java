package com.co.jic.template.market.controller;

import com.co.jic.template.market.DTO.ProductDTO;
import com.co.jic.template.market.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@CrossOrigin("http://localhost:4200")
@RestController
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @RequestMapping(value="/products", method = RequestMethod.POST)
    public ResponseEntity<?> saveProduct(@RequestBody ProductDTO productDTO) {
        try {
            ProductDTO productResponse = this.productService.save(productDTO);
            return ResponseEntity.ok(productResponse);
        }  catch (NullPointerException e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Try later");
        }
    }

    @RequestMapping(value="/products", method = RequestMethod.GET)
    public ResponseEntity<?> getAll() {
        try {
            List<ProductDTO> productResponse = this.productService.getAllProducts();
            return ResponseEntity.ok(productResponse);
        }  catch (NullPointerException e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Try later");
        }
    }

    @RequestMapping(value="/products", method = RequestMethod.PUT)
    public ResponseEntity<?> updapteoduct(@RequestBody ProductDTO productDTO) {
        try {
            ProductDTO productResult = this.productService.updateProduct(productDTO);
            return ResponseEntity.ok(productResult);
        }  catch (NullPointerException e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Try later");
        }
    }

    @RequestMapping(value="/products", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteProduct(@RequestParam String idProduct, @RequestParam String secondParam) {
        try {
            this.productService.deleteProduct(idProduct);
            return ResponseEntity.ok("Product was deleted");
        }  catch (NullPointerException e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Try later");
        }
    }

}
